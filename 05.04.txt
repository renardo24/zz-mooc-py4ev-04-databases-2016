So now what we're going to do is play a
little bit with the Google GeoCoding API. So we've got the Google GeoCoding API, we
have a list of the locations that we want to retrieve, and we want to write and use
a program to loop through these things and retrieve them one at a time and
start adding them to a database. And what's cool about this is we can blast
that and start over and it will look again and skip the ones it's already got and
only add the ones that are new. Because this has a limitation
of 2,500 per day, we don't want to waste our calls
to the Google GeoCoding API. And so we're very careful. We're going to start this, stop it. We might load like 20 and then visualize
them, and then load 20 more and visualize them, so we want to be able to
do this in a sort of a separate process. Then we'll play with the data once we
get it, we'll read it out of geodump, it'll sort of show us some of the data,
and then it'll create a file and we'll visualize that
file using the Maps API. The Google Maps API. This here
is already provided. So let's get to it. Let's get to it. So we start here. This comes in geodata.zip. You can download it from pythonlearn.com. where.data is the input
to this program and it's just the data that
came from the survey. People just typed in all
the different things in their survey. And then, geoload.py is where
the exciting things happen. And there's a few things
that we're good at. We know about json and urllib. And sqlite3 is the new
thing that we're going to import. Here is the base service URL,
the Google GeoCoding API. And then we're going to basically do
the equivalent of an open, right, where we're going to say,
let's create a connection. And a connection is sort of like
a file handle, and the file name, we're going to connect to an SQLite3 database
stored in the file named geodata.sqlite. And then we use a cursor as sort
of a sub-connection within that, so we say, hey,
dear connection give me a cursor and we're going to store that
in the variable cur. And we can call a method inside of
cur to execute a bit of SQL, and so this is just a bit of SQL,
and we are basically saying, hey, let's create a table,
only if it doesn't exist yet. If it does exist, don't do anything. We'll name the table Locations, and
we're going to put two columns in that table. One is an address column, and actually,
the other is the actual JSON geodata. And they're both text columns. And then we're going to start
retrieving these things from the API. We're going to open our input data
which is just one line per. And then we're going to loop through it. We're only going to let
ourselves do 200 at a time. You can take this out if you want. And we'll strip it,
the address is the entire line. And then what we're going to
do is we are going to select the geodata column from the locations
table with a WHERE clause. And we're going to use the logical key. And we are going to convert to
take the address that we've read, and buffer as a way to basically force it,
if it happens to be Unicode, we force it to what we want it to be. And then, this little syntax is a little
bit tricky in that this is a tuple, so the second parameter to
this execute is a tuple. And it really only wants to be one value,
because you need to put a value in for each of the little question
marks you put here. So this corresponds to that. If there was a second question mark, which we'll see in a second,
you'd have more things here. But you still have to put this
parentheses in to make it be a one-tuple. So this is the sub zero entry in
this tuple as the second parameter to the execute method. Okay? So what that does is that sends
a SELECT statement to the database. Database looks down and it uses this WHERE clause to see if,
yup, do I have it or not? Now, first time through we're not going to
have it because we just made the table. So what we do is after we run the select, we have to read the actual data that comes
back in the form of a series of rows. So the cur.fetchone, that's our cursor,
fetchone is the method, fetchone says get me one row. And the row ends up being a list,
we're only selecting one thing. But it's a list of things and so
the zero item is the geodata. And so that says grab me a row
using this cursor, and get the first column, right?
And bring it back in data. Now if that doesn't exist,
we're going to go to the except. If it does exist we're going to say "Found
in database" and we're going to continue. So what happens is if we are reading
things that are already in the database we're going to go like this and
it's going to happen really fast and we're not talking to Google at
all in this particular loop. Okay? But if there is no data, and
there will be no data on the first one, if there us no data it hits the exception, then it comes through with a pass and
continues on. This is what happens when there
is no data in the database. So the first thing it does,
it prints out a thing. We do a serviceurl, urllib.encode. You've done this before. And then we do a urlopen. If you have a problem, there's some
explanation in the readme about what to deal with if you get
a certificate problem here. And so we just open the URL and we read
the URL and then we retrieve some data, we print some stuff out, and then what we're going to do is check
to see if we got good data. When you're doing sort of like a spidering
like process you've got to check to see if we're good data. And so we're converting the data in
case it happens to be Unicode to string. We do a load s which is load string
using JSON from the json library. And what we're really trying to see is,
is this going to work or not? And that's why we put it into try/except. If it doesn't work, we're going to
just say blow up and continue. Because there's nothing we can
do because it's bad JSON. Once we got good JSON,
we can look for a status variable and that would be in the JSON. If there is no status, or
if the status is not OK, or if the status is zero results,
then we're going to complain. We're going to say "Failure to Retrieve"
and print it out and quit. This whole little pattern of if something
goes wrong print the data out and quit allows you to go back and
adjust the program a little bit. And when you're first building this,
you'll find that you need to put more intelligence in here, and
if something goes wrong, you blow up and then you start the program and
you run it again. But this one sort of works, and so we probably won't have to
do that too many times. Now, if we get all the way down to here,
at this point we have got the data which we read from the URL and
we're going to insert it into the database. So we're going to INSERT INTO the table
named Locations the address and the actual data, this is the actual
JSON data here, yada yada. Values, and then we have two
little question mark guys and so this here is the SQL command that
we're sending to the database and then we got to send a two-tuple in. So that's a tuple and the zero is the
address and the one is the actual data. So that is going to do an insert with two
parameters, the address and the data, and so that's going to add a row to the table. And then we have this thing called commit. And what happens is, for
performance reasons it doesn't write every single thing
that you're doing to the database, but when you say commit,
then it says now write it to the database. Actual file on disk. And what happens is, if we were to
blow up this program right here, then it's halfway done. The database would not
see those transactions. But if after we get to this point,
and then we're going to sleep and wait a second so we can go up, so
we're not doing this too fast. The commit basically says it's
all written to disk, and so if the program blows up at some point,
we're not going to lose any data. So that's one of the things
we're kind of appending to this. Committing.
Insert. Commit.
Insert. Commit.
Insert. Commit.
And blow up. Well, the data, all the inserts
will be on that actual file. And so, it makes sure the file works
even in the case. And so, this loop, we're actually
going to blow it up, right? We're not actually going to run it. If it ran for 200 it could run for 200.
Okay? So that's basically the code that we're
dealing with, so let's go ahead and try to run that. Okay, so here we are. Oops, go back. Here we are. And I grabbed the geodata.zip,
and I've unzipped it. And right now I've got some files in here. A where.data,
all the files that I showed you. So now I'm going to run python geoload,
okay? And it's going to create a database and then it's going to start reading from
where.data and it's going to, for each one, check to see if it's in the database and
if it's not it's going to call Google. So here we go, it's calling Google, called Google,
waiting a second between each one. Now, I can hit Ctrl+C. Ctrl+C is the way you blow up
a terminal program, I just blew it up. And I can, the Ctrl+Z is how you
do it on Windows and Ctrl+C is how you do it on
Mac and Linux. Okay? And so I blew this up, but there is
now a database. ls, geodata.sqlite. Well, let's check to see
what's in the database. And so we are going to go right here,
and we have this file geodata.sqlite. It's down here, code/pythonlearn/geodata,
wherever you put this file, and geodata.sqlite. So let's take a look. So now we'll do Browse Data. So now what we have,
make these a little bigger. Look at that. Happy little. Okay? And so this is the JSON that came back. And so what you see is you see
Northeastern University, Hong Kong, Technion. I don't know where these things
are, but then we have the data, right? So the data is being accumulated. So now what we're going to do
is we're going to run it again. And we'll see that when we run it again, let me clear this,
it's going to read from all of them. But it's first going to check the,
before it retrieves it, it's going to check if it's already got it. So watch what happens. It's going to go really quick. And show like, oh found it, found it,
found it, found it, found it. And quickly get to the end where it's actually got to add new data. So here we go. Found it. Found it. Found it. See, it found it.
Right? Found it in the database, and
now it's retrieving new stuff. And we could let it run for
a little while. Something could go wrong,
you could close your laptop and then it blows up. So let's go back,
see what's in the database. I can hit Refresh on this, and see
our database keeps growing. Okay? And if I do this one more time,
I can run it again, and you'll notice that it zooms
through the ones it already has. So it's not hitting Google at all. So we're saving our 2500. So now it's on to the new ones.
Okay? So you get the idea. So now I'm going to hit Ctrl+C.
Okay? And I'm going to hit Refresh here. And you see that it just keeps
adding more and more files. More and more locations to our
little database. So the next thing we're going to do is
we're going to run the file geodump.py. So we're filling the database up. We have this crawler that
sort of starts and restarts. And so we're going to just connect to
the database, get ourselves a cursor, and we're going to execute 
a SELECT * FROM Locations, which just basically is
going to retrieve all the rows. We're going to open a file where.js
write with a UTF character set. And then we're going to start
writing some code. And we're going to write actually some JSON. Don't worry too much about this. We'll show you how this
turns out in a bit. And then, this cursor,
the cursor like keeps track of where in, so here's the database, and
here's a bunch of records in the database. The cursor can be repeatedly go down
each of the records in the database and that's what we're going to do. for row in cursor, so
row is an iteration variable, it's going to go through each
of the rows in that table. And row is going to be a two-dimensional
array with positions 0 and 1, where 0 is the address and
1 is the actual data. Okay? And so, we're going to grab this,
the row, convert it to a string. We're going to read it and json load it. If we get a JSON error,
we're going to just go back up. If not, we're going to check to
see if the status is okay. If not, go back. But then, we're going to do exactly what
we did in that previous assignment, we're going to pull out of that JSON but
this JSON is now coming from our database. And we're going to get the formatted
address and check the sanity. We're going to replace single
quotes with nothing. And when it's all said and
done we are going to print out in a particular format. we're
going to print out the latitude, the longitude, and the location,
and then write that out. So this is actually writing a file,
but it's probably just as easy for me to run it and
then show you what the file looks like. But it basically is producing
a file in a particular format. So if I run python geodump,
21 records were written to where.js, it tells us to open where.html
to view the data in the browser, and it's dumping data as it sees it. So if I take a look at where.js,
this is a format called JSON, which is JavaScript Object Notation, well,
of course we talked about that before, except now we're going to
actually use it in JavaScript. And so, this is just JSON data
that's an array of arrays, and it is the GPS coordinates and
the name of the place. But this is the name of the place that
came from, these are the names of the places that came from the Google API as compared
to coming from the end user. So we kind of cleaned up the user's data. And so we have an array of arrays. And that, if you then open where.html, we could do this in my browser. And so it's giving me the OK. If you're an HTML expert, you could
take a look at this stuff, and so this is using the Google map,
and it's reading that JSON data. Don't worry too much about this. You can look at it as much or
as little as you want. It should just work. Okay? And so, here we have
those first 21 places. Now here's the interesting
thing that we can do. Let me re-size this a little bit. The interesting thing we can do now
is we can go and do this again. We can say python geoload.py, that's pulling data and
putting it into our database. And we'll let that run for a while. It sees that some things are there. We'll just run and
put a few more results in. Okay, and I'll just hit Ctrl+C,
and it blows up. But that's not really
a problem because I blew it up. And then I can run python geodump, and
that's taking the data from the database and updating the JSON stuff, it's where.js
and you actually see this right here. It's got more data. So that file's now got new data. And if I go back to my browser,
I should be able to hit Refresh and you'll see a bunch of new pins popping up. So yeah. There are more pins. Okay? So this is one of those things
where you can start it, you can run it, you can restart it,
etc., etc., etc. And so I just wanted to run you
through that. And then you can go ahead and
play with this on your own.