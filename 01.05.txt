Now we're going to talk about
a thing called "object lifecycle". Now actually I've been talking
about it all along. Every time I use the word construct
I'm talking about object lifecycle. We're just going to dig
a little deeper and understand exactly what's happening
when objects are being constructed. Because it's actually something that we can
render an opinion as we build an object, or as we build a class, as to hey,
when this thing is constructed, I want to do this stuff. So we make them, we throw them away,
we work with them. And we have special blocks of code,
specially named methods, that get called at the moment
that things get created and at the moment that
this variable goes away, they are called the constructor and
the destructor. We use constructors all the time,
and destructors are sort of rarely used, and just rarely used. But we're going to print them out anyways. So the primary purpose of the constructor
is to have you have some instance variables like the x equals or whatever,
to set them up with initial values so the instance of the object can be quite
different, as of the moment of creation. So here we have this class, right? And the code we had from
before is right there. And now we've got two new methods, and we have the __init__. And __del__. All the methods that have, well most of the methods that have
double underscores are used for certain code that gets run when
certain things happen in the language. And so that, init says when this creation, when the constructor's
called, this code gets run. And then we're not getting rid of it. If we got rid of it with a delete or
a del, then we could. We'd see it, but the only time this is going to get
destructed is at the end of the program. And so when this variable vanishes,
we're going to watch it be destructed. And we're registering by putting these
methods in that we want to run code at the moment of construction and
at the moment of destruction. We are defining the object, defining the class to say I
want these things to happen. I want to capture this. So this execution is quite simple. So it just reads this, right? Then it says let's
construct a PartyAnimal, which means that automatically because
this happens it's going to run this code. And self is the new instance, and so we
can actually look at instance variables, we don´t have to do anything here, all
we´re going to do is print I am constructed. So, this line right here causes
I am constructed to print out. Then the object exists. We´re going to call its party method
three times, prints out 1, 2, 3, just like we did before, and then the
program ends and this variable goes away. But we have no more prints. But because this an is going away, not
because PartyAnimal is going away, but because an is going away at the end of the
program, it says you asked me to run this code whenever I'm deleting
your ultimate variable. And so we get to print that out, and
we get to see the self variable. And that is the instance variable
that is x, which the 3, which was the last version of it. So, this is sort of like the moment of
creation and the moment of destruction of this variable are things that we can
choose to write code to jack into that. To say I'm jacking into this
moment of construction, and I'm jacking into
the moment of destruction. Let's not worry about
why you would do that. Like I said, this is rare, and
this is very common. The constructor's very common and
the destructor's very rare. Usually what's happening here is you're
setting up some of these instance variables in the constructor
to get it right. And we'll see an example of that, we won't really see an example of
a useful destructor, in a second. Okay? Okay, constructor is a special block of
statements that's called when the object is created. So, remember that we have classes and then we use classes to go
[SOUND] make these things. Each of the instances has its own
copy of the instance variables. That's what we're going to talk about now. Own copy of the instance variables. That’s what we're going to
hit on at this point. Now, everything we've done
up to now is one instance. Although when we write lots of strings,
there's many instances there, but the class PartyAnimal,
we've only made one instance of. Okay, so now what we're going to do is
we're going to say, you know what? We're going to create two instance
variables, x and name, and name is going to start out as empty. And we're going to create a constructor. We have no destructor. Destructors are pointless now. And we're going to say that
this constructor is going to have one parameter because self is
sort of added by Python at that moment. So if you want one parameter in your
constructor you need two parameters in init, and the first parameter is always
self which is our alias to the instance that we're currently in. And so what happens now is of course it runs
this code, does absolutely nothing except remembers the shape of party animals.
Right? And now we're going to
make one of these things. Pass in Sally. So Sally becomes nam. And this is a super, super common thing
to do in object oriented constructors, where you just copy this parameter,
which will vanish momentarily. But because we now have an object, we can copy this into self.name,
putting Sally into that, okay? And then we call the method party once. Then we make a second
instance in the variable j. And then that calls again, but there is
another instance that's been created. Okay, so let's take a look
at this in execution. So start out,
it just reads all this stuff. Now it's going to actually create the first
instance of the class PartyAnimal. And so that basically, that sort of
starts us at this point. And then we're going to
take that parameter, whatever that first parameter is, and then
we're going to copy it into name right here. So that copied it. See this is now an object. Its alias is s, and self is another
alias that can be used inside there. And then print out the print statement,
so out comes the print statement. We won't show that. And then we move to
the call of the method. So s is one object, and
then we call the party method. And that runs this code right here. And self of course, in this case,
self points to s, okay? And so it runs it and it prints the self
name which is going to be Sally, right? And then it adds 1 and
updates this to 1. Then it comes back down to here and
it's going to do the next statement. So the next statement is going to call
the constructor again, right? So it's going to call the constructor again and
say let's make a new PartyAnimal. In many languages, not Python,
they use the word new. And Jim is the variable that
we pass in as that parameter. So it runs. It says let's make a new object, and
let's initialize the name to be Jim. So the name in this object is Jim. So now we have two instance,
instance one and instance two. While this code is running
self is an alias to j. Self if no longer an alias to s, because
we are running in this new instance, okay? That code's the one that's running. So, we print that out, out comes a print
statement, and now we move to here. So now we have two instances. We have a variable s, and a variable j, so we call j.party which is going to
run this code right here, whoops. And self, for now,
is going to point at j because j is the instance of the object that we're
actually running the party method in. Both these objects have party methods,
it's just which one, and we know which one, because self comes in. So, self points to j, then the code runs. Code runs, it updates the variable here because
that's the one that self is pointing to. It prints out Jim 1 and
then it comes to the next line. Okay? So then again what it's
going to do is it's going to run. My animation's not working but
that's okay, I can animate by hand. It's going to run this,
except now self is pointing to s, because s is the variable that we're
calling the method in, self points to it. So then it's going to add this, make this
be 3, no actually make it be 2. Sorry, makes it be 2. Yeah, so that'll be 2. And then it runs and
it prints and it runs. So this is actually surprisingly simple. It's just that this code runs for
all of these. It’s shared between them. But depending on which one we're calling
it from, self either aliases to s or to j. And so that's the purpose
of this little self guy. Okay? So that is multiple
independent instances. And so we just sort of hit these
definitions again and up next we'll sort of wrap
this all up.